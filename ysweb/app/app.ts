﻿namespace fabric {
    export var Background;
    export var Grammar;
    export var Header;
}

fabric.Background = fabric.util.createClass(fabric.Rect, {
    type: "background",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
fabric.Grammar = fabric.util.createClass(fabric.Rect, {
    type: "grammar",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
fabric.Header = fabric.util.createClass(fabric.Rect, {
    type: "header",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
class Ys {
    app = new fabric.Canvas(document.getElementById("c") as HTMLCanvasElement);
    private headerHeight;
    background;
    header;
    constructor() {
        window.addEventListener("resize", this.resize);
        this.resize();
        this.app.renderOnAddRemove = true;
        this.headerHeight = this.app.getHeight() / 12;
        this.app.on("mouse:over", e => {
            let result = "default";
            switch (e.target.get("type")) {
                case "grammar":
                    result = "crosshair";
                    break;
            }
            e.target.hoverCursor = result;
        });

        this.drawBackground();
        //this.drawHexagram();
        this.drawHeader();
        this.drawTextInput();
    }
    renderHexagram = () => {
        //const linecount = 12;
        //let delay = 100;
        //for (let i = 0; i < linecount; i++) {
        //    const idx = i;
        //    setTimeout(() => {
        //        if (idx % 2 === 0) {
        //            const rectOptions = {
        //                width: background.width - (background.width / 10),
        //                height: (background.height / linecount),
        //                top: - (background.height / linecount),
        //                left: (background.width - (background.width - (background.width / 10))) / 2,
        //                fill: "#000000",
        //                selectable: false
        //            } as fabric.IRectOptions;
        //            const r = new fabric.Rect(rectOptions);
        //            this.app.add(r);
        //            r.animate("top", rectOptions.height * idx + (background.height / linecount / 2), {
        //                duration: 1000,
        //                onChange: this.app.renderAll.bind(this.app),
        //                onComplete: () => { },
        //                easing: fabric.util.ease["easeInOutBack"]
        //            });
        //        }
        //    }, delay);
        //    delay += 100;
        //}

    }
    resize = () => {
        const w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        const h = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        const width = w;
        const height = h;
        this.app.setDimensions({
            width: width,
            height: height
        });
    };
    drawBackground = () => {
        const backgroundOptions = {} as fabric.IRectOptions;
        backgroundOptions.width = this.app.getWidth();
        backgroundOptions.height = this.app.getHeight();
        backgroundOptions.selectable = false;
        const background = new fabric.Background(backgroundOptions);
        const gradientOptions = this.createVerticalGradientOptions("linear",
            this.app.getHeight(), { 0: "#FAEDC8", 1: "#ffffff" }) as fabric.IGradientOptions;

        background.setGradient("fill", gradientOptions);
        this.background = background;
        this.app.add(background);
    }
    drawHexagram = () => {
        const linecount = 12;
        let delay = 100;
        for (let i = 0; i < linecount; i++) {
            const idx = i;
            setTimeout(() => {
                if (idx % 2 === 0) {
                    const rectOptions = {
                        width: this.background.width - (this.background.width / 10),
                        height: (this.background.height / linecount),
                        top: - (this.background.height / linecount),
                        left: (this.background.width - (this.background.width - (this.background.width / 10))) / 2,
                        fill: "#000000",
                        selectable: false
                    } as fabric.IRectOptions;
                    const r = new fabric.Grammar(rectOptions);
                    this.app.add(r);
                    r.animate("top", rectOptions.height * idx + (this.background.height / linecount / 2), {
                        duration: 1000,
                        onChange: this.app.renderAll.bind(this.app),
                        onComplete: () => { },
                        easing: fabric.util.ease["easeInOutBack"]
                    });
                }
            }, delay);
            delay += 100;
        }
    }
    drawHeader = () => {
        const headerOptions = {} as fabric.IRectOptions;
        headerOptions.width = this.app.getWidth();
        headerOptions.height = this.headerHeight;
        headerOptions.stroke = "#333333";
        headerOptions.strokeWidth = 1;
        headerOptions.selectable = false;
        const header = new fabric.Header(headerOptions);
        header.setGradient("fill", this.createVerticalGradientOptions("linear", this.headerHeight,
            { 0: "#333333", 1: "#222222" }));
        this.header = header;
        this.app.add(header);
    }
    drawTextInput = () => {
        let p1 = new fabric.IText("Make your question below",
            {
                top: this.headerHeight + ((this.app.getHeight() - this.headerHeight) / 4),
                left: this.app.getWidth() / 4.5,
                width: this.header.width / 2,
                fill: "#333333",
                fontSize: 45,
                //textBackgroundColor: "#333333",
                hasBorders: false,
                lockMovementX: true,
                lockMovementY: true,
                hasControls: false,
                selectable: false,
                textAlign: "center"
            } as fabric.ITextOptions);

        let p = new fabric.IText("  ",
            {
                top: this.headerHeight + ((this.app.getHeight() - this.headerHeight) / 3),
                left: this.app.getWidth() / 3,
                width: this.header.width / 2,
                //height: (this.app.getHeight() - this.headerHeight) / 3,
                backgroundColor: "ffffff",
                textBackgroundColor: "#333333",
                hasBorders: true,
                lockMovementX: true,
                lockMovementY: true,
                hasControls: false,
                selectable: false,
                textAlign: "center"
            } as fabric.ITextOptions);
        this.app.add(p1);
        this.app.add(p);

    }

    private createVerticalGradientOptions(type: string, height: number, colorStops: any): fabric.IGradientOptions {
        return {
            type: type,
            x1: 0,
            y1: 0,
            x2: 0,
            y2: height,
            colorStops: colorStops
        } as fabric.IGradientOptions;
    }
}
var ys = new Ys();