var fabric;
(function (fabric) {
})(fabric || (fabric = {}));
fabric.Background = fabric.util.createClass(fabric.Rect, {
    type: "background",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
fabric.Grammar = fabric.util.createClass(fabric.Rect, {
    type: "grammar",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
fabric.Header = fabric.util.createClass(fabric.Rect, {
    type: "header",
    initialize: function (options) {
        options = options || {};
        this.callSuper("initialize", options);
    },
    _render: function (ctx) {
        this.callSuper("_render", ctx);
    }
});
var Ys = (function () {
    function Ys() {
        var _this = this;
        this.app = new fabric.Canvas(document.getElementById("c"));
        this.renderHexagram = function () {
            //const linecount = 12;
            //let delay = 100;
            //for (let i = 0; i < linecount; i++) {
            //    const idx = i;
            //    setTimeout(() => {
            //        if (idx % 2 === 0) {
            //            const rectOptions = {
            //                width: background.width - (background.width / 10),
            //                height: (background.height / linecount),
            //                top: - (background.height / linecount),
            //                left: (background.width - (background.width - (background.width / 10))) / 2,
            //                fill: "#000000",
            //                selectable: false
            //            } as fabric.IRectOptions;
            //            const r = new fabric.Rect(rectOptions);
            //            this.app.add(r);
            //            r.animate("top", rectOptions.height * idx + (background.height / linecount / 2), {
            //                duration: 1000,
            //                onChange: this.app.renderAll.bind(this.app),
            //                onComplete: () => { },
            //                easing: fabric.util.ease["easeInOutBack"]
            //            });
            //        }
            //    }, delay);
            //    delay += 100;
            //}
        };
        this.resize = function () {
            var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            var h = (window.innerHeight > 0) ? window.innerHeight : screen.height;
            var width = w;
            var height = h;
            _this.app.setDimensions({
                width: width,
                height: height
            });
        };
        this.drawBackground = function () {
            var backgroundOptions = {};
            backgroundOptions.width = _this.app.getWidth();
            backgroundOptions.height = _this.app.getHeight();
            backgroundOptions.selectable = false;
            var background = new fabric.Background(backgroundOptions);
            var gradientOptions = _this.createVerticalGradientOptions("linear", _this.app.getHeight(), { 0: "#FAEDC8", 1: "#ffffff" });
            background.setGradient("fill", gradientOptions);
            _this.background = background;
            _this.app.add(background);
        };
        this.drawHexagram = function () {
            var linecount = 12;
            var delay = 100;
            var _loop_1 = function(i) {
                var idx = i;
                setTimeout(function () {
                    if (idx % 2 === 0) {
                        var rectOptions = {
                            width: _this.background.width - (_this.background.width / 10),
                            height: (_this.background.height / linecount),
                            top: -(_this.background.height / linecount),
                            left: (_this.background.width - (_this.background.width - (_this.background.width / 10))) / 2,
                            fill: "#000000",
                            selectable: false
                        };
                        var r = new fabric.Grammar(rectOptions);
                        _this.app.add(r);
                        r.animate("top", rectOptions.height * idx + (_this.background.height / linecount / 2), {
                            duration: 1000,
                            onChange: _this.app.renderAll.bind(_this.app),
                            onComplete: function () { },
                            easing: fabric.util.ease["easeInOutBack"]
                        });
                    }
                }, delay);
                delay += 100;
            };
            for (var i = 0; i < linecount; i++) {
                _loop_1(i);
            }
        };
        this.drawHeader = function () {
            var headerOptions = {};
            headerOptions.width = _this.app.getWidth();
            headerOptions.height = _this.headerHeight;
            headerOptions.stroke = "#333333";
            headerOptions.strokeWidth = 1;
            headerOptions.selectable = false;
            var header = new fabric.Header(headerOptions);
            header.setGradient("fill", _this.createVerticalGradientOptions("linear", _this.headerHeight, { 0: "#333333", 1: "#222222" }));
            _this.header = header;
            _this.app.add(header);
        };
        this.drawTextInput = function () {
            var p1 = new fabric.IText("Make your question below", {
                top: _this.headerHeight + ((_this.app.getHeight() - _this.headerHeight) / 4),
                left: _this.app.getWidth() / 4.5,
                width: _this.header.width / 2,
                fill: "#333333",
                fontSize: 45,
                //textBackgroundColor: "#333333",
                hasBorders: false,
                lockMovementX: true,
                lockMovementY: true,
                hasControls: false,
                selectable: false,
                textAlign: "center"
            });
            var p = new fabric.IText("  ", {
                top: _this.headerHeight + ((_this.app.getHeight() - _this.headerHeight) / 3),
                left: _this.app.getWidth() / 3,
                width: _this.header.width / 2,
                //height: (this.app.getHeight() - this.headerHeight) / 3,
                backgroundColor: "ffffff",
                textBackgroundColor: "#333333",
                hasBorders: true,
                lockMovementX: true,
                lockMovementY: true,
                hasControls: false,
                selectable: false,
                textAlign: "center"
            });
            _this.app.add(p1);
            _this.app.add(p);
        };
        window.addEventListener("resize", this.resize);
        this.resize();
        this.app.renderOnAddRemove = true;
        this.headerHeight = this.app.getHeight() / 12;
        this.app.on("mouse:over", function (e) {
            var result = "default";
            switch (e.target.get("type")) {
                case "grammar":
                    result = "crosshair";
                    break;
            }
            e.target.hoverCursor = result;
        });
        this.drawBackground();
        //this.drawHexagram();
        this.drawHeader();
        this.drawTextInput();
    }
    Ys.prototype.createVerticalGradientOptions = function (type, height, colorStops) {
        return {
            type: type,
            x1: 0,
            y1: 0,
            x2: 0,
            y2: height,
            colorStops: colorStops
        };
    };
    return Ys;
}());
var ys = new Ys();
//# sourceMappingURL=application.js.map